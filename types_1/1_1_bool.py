
# # Неизменяемые
# bool
# int
# float
# str
# tuple
#
# # Изменяемые
# list
# dict
# set


# Объекты хранятся в переменных

# Boolean - логический тип данных
first_variable = True
second_variable = False

# C помощью функции type можно получить тип любой переменной.
second_variable_type = type(second_variable)
# print(type(True))
# print(second_variable_type)


# В Python булевые значения могут обрабатываться как номера.
int_true = int(True)
int_false = int(False)

# print(int_true)
# print(int_false)

# Поэтому с ними можно выполнять математические операции.
result_sum = True + False
result_sum_true = True + True
result_div = True - True

# print(result_sum)
# print(result_sum_true)
# print(result_div)

# Можно проверить тип любой переменной. Это работат с любыми типами.
first = True
# is_bool = isinstance(isinstance(first, bool), bool)
# print(is_bool)


# Можно вывести сразу какое-либо значение, без присваевания.
# print(isinstance(first, str))


# Булевые операторы

# И (логическое умножение) and
# ИЛИ (логическое сложение) or
# инверсия not
# print(True or False)

is_str = isinstance(True, str)
is_bool = isinstance(True, bool)

# print(is_bool or is_str)

# a = 0 or 6
# print('a = 0 or 6', a)
# a = False or 6
# print('False or 6', a)
# a = isinstance(True, str) or 6
# # print('isinstance(True, str) or 6', a)
#
# b = False and 5
# print('False and 5', b)



# исключающее ИЛИ (сложение с переносом) ^


# эквивалентность (равенство) ==
# сравнение (>, <, <=, >=)

# print(True or False)
# print(False or False)
# print(True and False)
# print(not False)
# print(not True or False)


# None - особое значение. Отсутствие какого-то значения.
none_value = None
# print(type(none_value))

user = None
print(user is None)


