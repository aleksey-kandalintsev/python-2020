# функция, которая принимает другую функцию в качестве аргумента
def run_with_information(fn, *args, **kwargs):
    print('Начало выполнения')
    print('Неименованные аргументы: {}'.format(args))
    print('Именованные аргументы: {}'.format(kwargs))
    result = fn(*args, **kwargs)
    print('Окончание выполнения')
    print('Результат: {}'.format(result))
    return result


def sum_elemenst(n):
    return sum(range(n + 1))


# run_with_information(sum_elemenst, n=10)



















def print_information(fn):
    def wrapper(*args, **kwargs):
        print('Начало выполнения')
        print('Не именнованые аргументы: {}'.format(args))
        print('Именнованные аргументы: {}'.format(kwargs))
        result = fn(*args, **kwargs)
        print('Окончание выполнения')
        print('Результат: {}'.format(result))
        return result

    return wrapper


new_sum_element = print_information(sum_elemenst)


# print(new_sum_element(11))




# Синтаксический сахар для работы с декораторами.
@print_information
def sum_elemenst(n):
    return sum(range(n + 1))


# sum_elemenst(n=3)
































# Декоратор с аргуметами
def if_more(num):
    def decorator(fn):
        def wrapper(x):
            if x < num:
                print('{} меньше {}'.format(x, num))
                return
            else:
                return fn(x)

        return wrapper
    return decorator

@if_more(10)
def sum_elemenst(n):
    return sum(range(n + 1))


# result = sum_elemenst(15)
# print(result)










@print_information
@if_more(12)
def sum_elemenst(n):
    return sum(range(n + 1))

@if_more(34)
def minus_elem():
    pass
# result = sum_elemenst(222)
# print(result)








# Декортаторы так же работают с классами
def yeah_new_object(cls):
    def wrapper(*args, **kwargs):
        print('YEAH!! THIS IS NEW OBJECT!')
        return cls(*args, **kwargs)
    return wrapper



@yeah_new_object
class MyClass:
    pass


c = MyClass()


