# Словари

# Ключи должны быть хэшируемого типа (hashable),
# что означает, что в качестве ключа используют хэш значения ключа,
# который никогда не меняется в течение срока жизни ключа.
# Хэшируемые типы данных являются неизменяемыми объектами: int, str, tuple,
#

d = {
    'key1': 'value1',
    2: 'Value2',
    'key3': [1, 2, 3],
    (1, 4): 4
}
d1 = dict()
# print(type(d))
# print(d)

d = dict(key1='value1', key2='value2')

l1 = (1, 2, 3)
l2 = ('a', 'b', 'c')
zipped = zip(l1, l2, l2, l2)
# print(list(zipped))

dict(zip((1, 2, 3), ('a', 'b', 'c')))
# print(dict(zipped))
# получение значений по ключу

# print(d['key1'])
# name = 134444
# print(d.get(name, 'd'))











man = {'Имя': 'Вася', 'Фамилия': 'Сидоров'}
man['Возраст'] = 3
# print(man)

man['Возраст'] = 4
# print(man)



# Словарь может хранить разные значения
c = ['yellow', 'blue', 'green']
d = {
    'colors': c,
    'numbers': [1, 4, 5],
    'user': {'Имя': 'Вася', 'Фамилия': 'Сидоров'}
}
# print(d)
# print(d['colors'][1])


# del d['colors']
# print(d)
# del d['colors'][2]
# print(d)
# print(d.pop('user'))
# print(d)






# Длина словаря
l = len(d)

l = {
    1: 5,
    True: 4,
    1.0: 6
}
# print(l)

# l = {
#     1: 5
# }
# l[True] = 4
# l[1.0] = 6


# print(len(l))

# print(l[1])







# Получение ключей словаря

d = {
    'colors': ['yellow', 'blue', 'green'],
    'numbers': [1, 4, 5],
    'user': {'Имя': 'Вася', 'Фамилия': 'Сидоров'}
}

import pprint

# print(d.keys())
# print(d.values())
# pprint.pprint(d.items())
# pprint.pprint(list(zip(d.keys(), d.items())))



d = {
    'colors': ['yellow', 'blue', 'green'],
    'numbers': [1, 4, 5],
    'user': {'Имя': 'Вася', 'Фамилия': 'Сидоров'}
}

# Проверка ключей в словаре

result = 'colors' in d
result2 = 'key1' not in d
# print(result)
# print(result2)





# Изменяемый тип данных.

d = {'key1': 'Hello', 'key3': 1, 'key2': [1, 2, 3], 'num': 4}
d1 = {'key1': 'Hello', 'key3': 1, 'key2': 6, 'num': 5}
d2 = {'key1': 'Hello', 'key3': 1, 'key2': 6, 'num': 0}
l = [d, d1, d2]


sorted(l, key=lambda x: x['num'])

a = d
a = d.copy()  # dict(d)
# deepcopy()

# Что будет в словаре d если изменить значение 'key2' у a?

# print(a)

a['key2'] = 'world'
# print(d)












d = {'key1': 'Hello'}
a = d.copy()
# print(a)
a['key2'] = 'world'
# print(d)
# print(a)




# Безопасный способ получить элемент
d = {
    'city': 'Moscow',
    'country': 'Russia'
}
val = d.get('town', 'test')
# print(val)












# Генерация словарей

new_d = {x: x**2 for x in range(12)}
# print(new_d)



















# Словарь в булевом виде

print(bool({}))
print(bool({1: 3}))







# Итерация по словарю















# Другие коллекции и :
# https://docs.python.org/3/library/collections.html
