# Безымянные функции
import os

func = lambda x: x ** x

# print(type(func(2)))






equals  = lambda arg1, arg2: arg1 == arg2
# print(func(1, 99))


def f(x, y): return x + y

d = {
    'plus': f,
    'minus': lambda x, y: x % y + 1 + 3+ 1,
    'mult': lambda x, y: x * y,
}
print(d)
print(d['plus'](4, 5))
print(d['minus'](4, 5))
print(d['mult'](4, 5))
print(d)




# utils.py

def list_of_files(dir: str):
    return [os.path.join(dir, fname)
            for fname in os.listdir(dir)]


# process.py

xsd_files_dir = 'xsd'
# Отбор файлов с расширением .xsd
filter(lambda f: '.xsd' in f, list_of_files(xsd_files_dir))

filtered = []
for filename in list_of_files('functions_3'):
    if '.mp3' in filename:
        filtered.append(filename)

input_products = [
    {
        'name': 'огурец',
        'price': '50',
    },
    {
        'name': 'помидор',
        'price': '60',
    },
    {
        'name': 'свекла',
        'price': '30',
    },
]
map(lambda p: {'name': p['name'].title(), 'price': p['price']}, input_products)


sorted(input_products, key=lambda item: item['name'])

import functools

x = 3
value = 'true' if x ** 2 == 4 else 'else'

prices = [50, 60]
# На первой итерации в acc находится первый элемент последовательности,
# в el второй элемент последовательности
functools.reduce(lambda acc, el: acc + el, prices)

sum = functools.reduce(lambda sum, product: sum + int(product['price']), input_products)

sum = functools.reduce(lambda sum, product: sum + int(product['price']) if isinstance(sum, int) else int(product['price']), input_products)

# Решение:

def acc_value(acc):
    """ Выводит переданный элемент и возвращает. Для отладки"""
    print(acc)
    return acc


functools.reduce(lambda acc, el: acc + int(el['price']) if not acc_value(acc) else int(acc['price']) + int(el['price']), input_products)

# Решение:

functools.reduce(lambda acc, el: acc + int(el['price']) if isinstance(acc, int) else int(acc['price']) + int(el['price']), input_products)

# Та же логика в виде цикла
acc = input_products[0]
for el in input_products[1:]:
    print(el)
    if isinstance(acc, int):
        acc += int(el['price'])
    else:
        acc = int(acc['price']) + int(el['price'])

sum = 0
for product in input_products:
    sum += int(product['price'])




# HW
# Написать декоратор, для подсчета времени выполнения функции.

import datetime

d = datetime.datetime.now().timestamp()



