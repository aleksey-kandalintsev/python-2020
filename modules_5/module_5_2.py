# Импорты своих модулей
# import functions

# print(dir(functions))

# Важно помнить! Импортируемый файл всегда исполняется при импорте.
from functions import fib

import math

# print(math.asin(1, 2))
# print(fib(3))


# Пути для импорта.
import sys
import pprint
from pprint import pprint
# pprint.pprint(sys.path)

print(sys.modules)