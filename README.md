# MEDIASOFT: Базовый курс Python

В этом репозитории будет выкладываться код с лекций.

## Для выполнения заданий требуется:
* Установленный Python третьей версии. Скачать можно [отсюда](https://www.python.org/downloads/)
+ Репозиторий git. В [github](https://github.com/) или [BitBucket](https://bitbucket.com).  
 Как установить git можно прочитать [здесь](https://git-scm.com/book/ru/v2/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-Git)
