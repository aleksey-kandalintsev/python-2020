

def fib(n):
    a, b = 0, 1
    while b < n:
        print(b)
        a, b = b, a+b
    print()


def multiply_string(string):
    return string * 3


def cut_string(string):
    return string[1:-1]


def invert_string(string):
    return string[::-1]


print('My name is {}'.format(__name__))
