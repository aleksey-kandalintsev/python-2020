# Итератор

# https://devpractice.ru/python-lesson-15-iterators-and-generators/
# https://www.datacamp.com/community/tutorials/python-iterator-tutorial?utm_source=adwords_ppc&utm_campaignid=1455363063&utm_adgroupid=65083631748&utm_device=c&utm_keyword=&utm_matchtype=b&utm_network=g&utm_adpostion=&utm_creative=332602034364&utm_targetid=dsa-429603003980&utm_loc_interest_ms=&utm_loc_physical_ms=1012067&gclid=EAIaIQobChMIuK-U4_zx6QIVRpSyCh1WowYrEAAYASAAEgKCuvD_BwE
# https://nvie.com/posts/iterators-vs-generators/

import csv

l = [1, 3, 4, 5]

l_iterator = iter(l)
print(type(l_iterator))
#
print(next(l_iterator)) # l_iterator.__next__()
print(next(l_iterator))
print(next(l_iterator))



for x in l_iterator:
    # print(x)
    pass














# Генераторы

import random


def randoms(count):
    for x in range(count):
        yield random.randint(0, 200)
        print(x)

list(randoms(10))

#
# # for r in randoms(6):
# #     print(r)
# #     pass
#
# print(type(randoms(6)))
#
#
# f = randoms(6)
# print(next(f))
# print(next(f))

# Какого типа?
# import os
files = (f for f in os.listdir(os.getcwd())) # files = (f for f in os.getcwd())
# type(files)
files = [f for f in os.listdir(os.getcwd())] # files = (f for f in os.getcwd())
#type(files)







def my_range(start, end, step):
    x = start
    while x < end:
        yield x
        x += step


for x in my_range(0, 100, 15):
    print(x)
    pass




# Примеры:


def process_data():
    pass


# загружает в озу весь файл
with open('img.jpeg', 'rb') as img:
    pass



def read_in_chunks(file_object, chunk_size=1024):
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 1k."""
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield data


# Если в файле есть разделители строк (\n), он построчный
# csv, xml, текстовые файлы, как правило
for line in open('really_big_file.dat'):
    process_data(line)


# https://stackoverflow.com/questions/519633/lazy-method-for-reading-big-file-in-python
# https://realpython.com/introduction-to-python-generators/
# https://stackabuse.com/read-a-file-line-by-line-in-python/

