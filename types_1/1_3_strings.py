# Все строки в юникоде
# Объявление строки

# Класс (тип) str

# string = ''
# string = ""

# string = str(555)

number_str = str(4)
# print(number_str)
# print(type(number_str))

single = 'single qoutes text'
double = "double \n \\ qoutes text"  # Экранирование

triple_single = '''triple single quotes text'''
triple_double = """triple

 single quotes textend"""

# print(single)
# print(double)
# print()
# print(triple_single)
# print(triple_double)


# Печатное представление строки.
# print(r'first row \n \ second row') # raw string

# Применяетя при написании регулярных выражений

# Конкатенация
concat_string = ("First part"
                 "Second part")
a = (1 +
     5)

# a = 1 + \
#     2
# print(concat_string)

# Операции со строками

# print("First part" + "Second part")

# При выполнении недопустимой операции выдаст ошибку
# print("First part" - "Second part")

# print("First" * 4)

# print('=' * 80)

streets = "ул. Марата, ул. Гагарина"

# Проверка вхождения
# print('ул. Марата' not in streets)

l = len("Строка")
# print(l)

# Поиск по строке
s = "  Hello world!"
# print(s.find("o"))
# print(streets.find('ул. Маратsа'))


# print(s.rfind("o")) # Поиск справа
# print(s[9])

# print(type(s.rfind("?"))) # Нет вхождений подстроки

# print("ул. Марата" in streets)
# print("ул. Марата" in "ул. Марата, ул. Гагарина")

# print("H" in s)

# # Проверки
# print(s.startswith('r'))
# print(s.endswith('!'))
# print("f".islower())
# print("f".isupper())
# print("4".isdigit())
# print("4adsf".isascii())

# Убираем пробел в начале строки
# print(s)
# print(s.lstrip())
# print(s.rstrip('!'))
# print(s)
# s = s.strip(' !')
# print(s)
# s = '""" # """'
# print(s.strip('" '))

# Заменяем символы. Важно получаем новую строку.
new_s = s.replace('Hello', '1')
# print(s)
# print(new_s)

# нижний ВЕРХНИЙ регистр
# print(s.lower())
# print(s.upper())

# Преобразование строки в список
s_point = 'a.b.a.c.1231231.'
# print(s_point)
# print(s_point.split('.'))

# 'Огурцы, Помидоры'
# print(streets.split(', '))


# Форматирование строк
format_string = 'Привет, %s. Сегодня %d.07.2020' % ('Иван', 13)
# print(format_string)

format_string_2 = 'Привет, {}. Сегодня {}.07.2020'.format('Михаил', 20)
# print(format_string_2)
# format_string_2.format()

format_string_3 = 'Привет, {name}. Сегодня {day}.07.2020'.format(day=22,
                                                                 name='Петр')
# print(format_string_3)

day = 22
name = 'Петр'
format_string_4 = f'Привет, {name}. Сегодня {day}.07.2020'
# print(format_string_4)
# f-strings

price = 205
name = 'Сыворотка из под простокваши'
universum = {'red', 'green', 'blue'}

formatted_float = "${:,.2f}".format(1500.2)

# print(f'{formatted_float!r} {price!s} {name!s} {universum!r}')


formatted_float = "${:,.2f}".format(1500.2)


# Срезы
s = "Don't panic!"
# print(s[0])

# print(s[2:5])

# print('01234567'[::3])

# print(s[-1:0:-1])

# print(s[::])

s2 = '012345'
# print(s2[::-2])

# конвернтация строк в булевые значения

# print(bool(''))
# print(bool('3123'))

# print(int('5555'))

# # Print
# print('a', 'b', sep='; ', end='') # separator
# print('a', 'b', sep='\t', end='\n')
#
# print('a', 'b', sep=', ', end='\t')

print('name = ', name, 'price = ', price, sep='|')
#
# total_balance = 250000
# personal_balance = 9420
# result = f'total_balance = {250000} personal_balance = {personal_balance}'
# print(result)
#
# # Использование .center, .ljust, .rjust в форматировании таблицы бюджета
table = [
    ['#', 'Name', 'Balance'],
    ['1', 'Игорь', 400.04],
    ['2', 'Петр Игоревич', 950.05],
    ['3', 'Иван', 3400.03],
    ['4', 'Анастасия', 3400.03],
]

# Гибкий подход
width = 20
for row in table:
    width = 20  # Почему не здесь?

    formatted_row = []
    for item in row:
        if isinstance(item, str) and item.isdigit():
            formatted_item = item.ljust(width)
        elif isinstance(item, str):
            item_str = str(item)
            formatted_item = item_str.center(width)
        elif isinstance(item, int) or isinstance(item, float):
            formatted_item = str(item).rjust(width)
        else:
            formatted_item = str(item)

        formatted_row.append(formatted_item)
        # formatted_row.append(item.center(20, '_'))
        # formatted_row.append(str(item).center(20, '_'))

    print(formatted_row)
#
# # Если уверены в количестве столбцов
for row in table:
    print('{:} {} {}'.format(row[0], row[1], row[2]))

    print(f'{row[0]}{row[1]}{row[2]}')


#
# # Статья по форматированию на русском:
# # https://pythonworld.ru/osnovy/formatirovanie-strok-metod-format.html
#
# # Официальная документация
# # https://docs.python.org/3/library/string.html#format-string-syntax
#
# # Официальная документация по строкам
# # https://docs.python.org/3/tutorial/introduction.html#strings
#
# # Полный список методов
# # https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str
#
# # Статья с примерами вызова основных методов:
# # https://medium.com/nuances-of-programming/строковые-методы-в-python-6a3e362020ec

# # И всегда есть Google и StackOverflow.
# # Google: Как вывести таблицу в Python?


# 'ё' > 'я'
# 'Ё' > 'а'
# 'Ё' < 'а'