# https://packaging.python.org/tutorials/packaging-projects/

# Подключение любого модуля из стандартной библиотеки.
# Как правило все используемые модули подключаются в начале файла.

# После подключения, получаем доступ для всех атрибутов модуля.
import datetime

from datetime import datetime
# print(datetime.datetime.now())


# Получаем доступ к указанному атрибуту модуля
from datetime import timedelta

# print(datetime.datetime.now() - timedelta(days=7))



# Можно задать алиас для модуля. Используется для удобства работы с большими именами.
import random as rdm
# print(rdm.randint(1, 20))





# Можно импортировать несколько значений
from functools import lru_cache, reduce

lru_cache, reduce


asin = 1

# Импорт всех атрибутов модуля
from math import *


import math

math.asin()

print(pi)












# Фаил __init__.py в каталоге говорит о том,
#  что все файлы в этом каталоге являются частью одного
#  модуля.

# В файле __init__.py может быть пустым
# или может определять основные "точки входа" в модуль.


from clases_4.test_4_1_classes import *

# print(type(clases_4.Fruit))


#
# import functions
# import functions_3
#
# print(functions)
# print(functions_3)