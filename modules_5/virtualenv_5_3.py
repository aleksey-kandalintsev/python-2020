# Виртуальное окружение

# Варианты:
#   * https://docs.python.org/3/library/venv.html
#       python -m venv ENV_DIR
#   * Установка https://virtualenv.pypa.io/en/stable/installation/
#
# Активация виртуального окружения:
#   source /path/bin/activate
#
# Установка пакетов:
#   https://packaging.python.org/tutorials/installing-packages/#installing-from-pypi

# Пример: pip install python-telegram-bot
# Доки по библиотеке https://github.com/python-telegram-bot/python-telegram-bot

# import telegram
# import pprint
# #
# bot = telegram.Bot('TOKEN')
# pprint.pprint([x.message.text for x in bot.get_updates(timeout=10)])




from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
updater = Updater('532130584:AAF8_JBgB1knsLkw7ow51Tz1Q2IDIqMxtq8')
dp = updater.dispatcher

d = {
    'is_start': False
}

def start(bot, update):
    """Send a message when the command /start is issued."""

    update.message.reply_text('Hi!')
    d['is_start'] = True

def check(bot, update):

    if d['is_start']:
        update.message.reply_text('bot is started')
        d['is_start'] = False
    else:
        update.message.reply_text('bot is not started')


dp.add_handler(CommandHandler("start", start))
dp.add_handler(MessageHandler(Filters.text, callback=check))

updater.start_polling()