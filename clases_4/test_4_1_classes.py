# Класс  -- это элемент, который описывает тип данных.
# Класс можно воспринимать как шаблон для создания объекта.


# Объект -- это экземпляр класса.


# Как обьявить класс.

# сlass Название_класса:
class Fruit:
    # атрибуты класса
    name = None
    color = None
    value = 1.0

    # Метод
    def __init__(self, name, color):
        self.name = name
        self.color = color

    # Метод
    def bite(self):
        if self.value > 0.0:
            self.value -= 0.3
            print('Укус на {}.'.format(0.3))
            return True

        return False




orange = Fruit(name='orange', color='orange')
apple = Fruit(name='apple', color='red')

# print(apple.name)
# print(apple.color)
# print(apple.value)


# apple.bite()

# print(apple.value)






# Наследование.
# При наследовании дочерний класс принимает все атрибуты и методы родительского класса.

class Peel:
    has_peel = True

    def clean_out(self):
        self.has_peel = False

# В скобках указываем классы, от которых хотим наследоваться.
# Порядок наследования важен!!!

class Citrus(Peel, Fruit):
    pass

    # Переопределение родительского метода.
    def bite(self):
        if self.has_peel:
            print('Нужно сначала очистить')
            return False

        # super -- вызовет метод у родителя.
        return super(Citrus, self).bite()


orange = Citrus(name='orange', color='green')


# dir -- все методы и атрибуты класса.
# print(dir(orange))

# print(orange.has_pell)
#
# print(orange.value)
# orange.clean_out()
# orange.bite()
# print(orange.value)















































# Магические методы. (Специальные методы)
# Методы класса, которы начинаются с __ и заканчиваются __
# и предоставляют возможость для взаимодействия с классом.

class A:
    pass

# print(len(dir(A)))

class Thing:
    def __init__(self, name):
        self.name = name


    def __str__(self):
        return self.name


    def __add__(self, other):
        return Thing(self.name + '-' +  other.name)


pen = Thing('pen')

string_thing = str(pen)
# print(string_thing)
# print(pen)
#
#
#
#
#
#

apple = Thing('apple')


# print(apple + pen + pen)
#
#
#
#
#
#
#
# #
# apple_pen = apple + pen
# pen_pineapple = pen + Thing('pineapple')
#
# print(pen_pineapple + apple_pen)









# Статические методы и методы класса

class DemonstarionClass:
    name = 'Default Name'

    def __init__(self, name):
        self.name = name

    # def __call__(self, *args, **kwargs):

    # Статический метод ничего не знает о классе и о объекте, к которому он принадлежит.
    @staticmethod
    def this_is_static():
        print('Static method')

    # Метод класса знает все про класс и ничего не знает об объекте.
    @classmethod
    def this_is_class(cls):
        print(cls.name)

# DemonstarionClass.this_is_static()
#
new_obj = DemonstarionClass('New Name')
new_obj.this_is_static()
new_obj.this_is_class()
DemonstarionClass.this_is_class()


new_obj.__class__()
def print_s(self):
    print(self.name)

DemonstarionClass.print_sd = print_s
# DemonstarionClass.name = '123123'
# new_obj = DemonstarionClass('New Name2')
# DemonstarionClass.this_is_class()
# new_obj.print_sd()








