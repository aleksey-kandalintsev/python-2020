# Обьявляем функцию

def my_sum(value_1, value_2):
    # if value_1 is None or value_2 is None:
    #     return None
    if None in (value_1, value_2):
        return

    result = value_1 + value_2
    return result


result_sum = my_sum(value_2=6, value_1=5)


# print(result_sum)


# Функция не всегда должна что-то возвращать

def empty_func():
    five = 2 + 2


def full_func():
    return '23'


# print(full_func())
# print(empty_func())


# Значения аргументов по умолчанию
def remove_symbols(string, unresolved_chars='1234567890'):
    new_string = ''
    for c in string:
        if c not in unresolved_chars:
            new_string += c

    return new_string


origin_string = 'Enter the Numbers: 4 8 15_16 23 42'


# print(remove_symbols(origin_string))
# print(remove_symbols(origin_string, 'eEu'))


# Значения аргументов по умолчанию. Изменяемые типы нельзя задавать как значение по умолчанию.
def increment_list(my_list):
    """
    Инкрементирует каждое число в списке.
    """

    for n in range(len(my_list)):
        my_list[n] += 1


new_list = [1, 4, 6, 7, 7, 7, 8]
increment_list(new_list)


# print(new_list)


# Какие значения получатся?







#
# print(magic_func())
# print(magic_func([]))
# print(magic_func())
# print(magic_func([1]))


# Если нужно значение по умолчанию для изменяемого типа, то нужно использовать None

def without_magic_func(l=None):
    if l is None:
        l = []
    l.append(2)
    return l


# Произвольный список аргументов
def print_multiple_args(prefics, *args):
    print(args)
    for i, a in enumerate(args):
        pass
        print('{} {}: {}'.format(prefics, i, a))


# print_multiple_args('Аргумент', 1, 23, 23, 1, 23123, 'test', {1:3})
# print_multiple_args('Аргумент', a=1, b=23, c=23, d=1, e=23123, f='test', g={1: 3})


# Произвольный список именованных аргументов
def print_multiple_args(prefics, *args, **kwargs):
    # print(type(kwargs))
    # print(args)
    for i, a in enumerate(kwargs):
        # pass
        # print(i)
        # print(a)
        print('{} {}: {}'.format(prefics, i, kwargs[a]))


# print_multiple_args('Аргумент', 1, 23, d=3)
# print_multiple_args('Аргумент', a=1, b=23, c=23, d=1, e=23123, f='test', g={1: 3})


# Функция как объект.
# Функции в питоне являются объектами. С функциями можно работать так же как и объектами.
def multiply_string(string):
    return string * 3


def cut_string(string):
    return string[1:-1]


def invert_string(string):
    return string[::-1]


# print(type(invert_string))
# print(callable(invert_string))

def do_with_string(s, *args):
    print(s)
    for f in args:
        if not callable(f):
            continue
        s = f(s)
        print(s)

# do_with_string('test string', multiply_string, cut_string, invert_string)
# print(isinstance('str', str))


"""
Импорты:
- examples/collections.py

- enter_file.py
- fake_main_file.py

https://docs.python.org/3/reference/import.html

"""


# Фишки:
# Таблица с дебагом: помогает проверить, все ли учтено. Плюс: наглядность.
# Упрощенная версия тестирования
# параметр 1, параметр 2, выход
#