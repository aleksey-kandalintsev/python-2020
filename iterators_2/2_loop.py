# Циклы

for n in range(5, 15, 2):
    # print(n)
    pass






# Цикл может итерироваться по любой коллекции
l = ['guido', 'van', 'rossum']
for w in l:
    # print(w)
    pass






#
# d = {1: 3, '2': 'sd'}
#
# for k, v in d.items():
#     print(k)
#     print(v)
#     print()



# Можно создавать вложенные циклы
for g in l:
    for w in '12345679':
        # print(g * int(w))
        pass











matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
for x in matrix:
    for el in x:
        # print(el)
        pass





# цикл while
i = 20
while i > 10:
    i -= 1
    # print(i)

# print('Result = {}'.format(i))

# while True:
    # print(1)


# Команды break и continue

# Прерывает выполнение цикла
for x in range(15):
    if x == 5:
        break
    # print(x)

# Переходит к след элементу
for x in 'Hello world':
    if x in ('e', 'o'):
        continue
        # pass
    # print(x)













#  else выполняется если функция завершилась без прерывания
names = ['Андрей', 'Иван', 'Марина', ' Кирилл']

for name in names:
    # print('Name: {}'.format(name))
    if 'Вася' == name:
        # print('Вася найден')
        break
    else:
        pass
else:
    # print('Мы не нашли Васю')
    pass


# Выполнится ли else, если не было ни одной итерации?
test = []
for name in test:
    pass
else:
    pass
    # print('?')













#  Итерируемся по словарю
bigest_city = {
    'Токио': 34500000,
    'Шанхай': 24280800,
    'Карачи': 23500000,
    'Пекин': 21516000,
    'Дели': 16349831,
    'Лагос': 16060303,
}

for x in enumerate(bigest_city.items()):
    # print(x)
    pass


for k, v in bigest_city.items(): #  for k, v in [('Токио', 34500000), ('Шанхай', 24256800), ('Карачи', 23500000), ...]
    pass


for v1, v2, v3 in [(1, 2, 3), (2, 3, 4), [3, 4, 5]]:
    # print(v1, v2, v3)
    pass


l = ['a', 'b', 'c', 'd', 'e']
for i in enumerate(l):
    # print(i)
    # print(x)
    # print()
    pass



# import
# files = os.listdir('functions_3')
files = []

for n, file in list(enumerate(files, 1))[:3]:
    pass










def find_item(lst, item):
    index = 0
    for x in lst:
        if x == item:
            break
        index += 1  # index = index + 1
    else:
        return -1
    return index









