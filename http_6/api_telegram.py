from urllib.parse import urljoin
import pprint
import requests

# Конфигурация
TOKEN = ''
HOST = 'https://api.telegram.org'
URI = '/bot{token}/{method}'


# Методы
def get_me():
    """ A simple method for testing your bot's auth token.
    Requires no parameters.
    Returns basic information about the bot in form of a User object.
    """
    method_name = 'getMe'

    # Просто собираем URL
    url = urljoin(HOST, URI.format(token=TOKEN, method=method_name))

    # Тоже самое что и:
    # url = HOST + URI.format(token=TOKEN, method=method_name)

    # print(url)
    response = requests.get(url)
    # print(response.status_code)
    return response.json()


result = get_me()
# pprint.pprint(result)



API_URL = f'https://api.telegram.org/bot{TOKEN}/'


def get_updates():
    """ Use this method to receive incoming updates using long polling (wiki).
    An Array of Update objects is returned.

    https://core.telegram.org/bots/api#getupdates
    """
    method_name = 'getUpdates'
    response = requests.get(API_URL + method_name)
    return response.json()



# result = get_updates()
# pprint.pprint(result)
#
# messages = [m['message'].get('text') for m in result['result']]
# pprint.pprint(messages)


def get_updates(update_id=None):
    method_name = 'getUpdates'

    url = urljoin(HOST, URI.format(token=TOKEN, method=method_name))

    if not update_id:
        response = requests.get(url)
    else:
        response = requests.get(url, params={'offset': update_id})
    return response.json()


# result = get_updates()
# pprint.pprint(result)
# #
# last_update_id = result['result'][-1].get('update_id')
# print(last_update_id)
# last_update_id = 875412119
# result = get_updates(last_update_id + 1)
# result = get_updates(last_update_id + 1)
# pprint.pprint(result)


from requests.exceptions import ReadTimeout


def get_updates(update_id=None):
    method_name = 'getUpdates'

    url = urljoin(HOST, URI.format(token=TOKEN, method=method_name))
    if not update_id:
        response = requests.get(url, params={'timeout': 10}, timeout=10)
    else:
        response = requests.get(url,
                                params={'offset': update_id, 'timeout': 90},
                                timeout=89)
    return response.json()
#
# last_update_id = 875412120
# result = get_updates(last_update_id + 1)
# result = get_updates(last_update_id + 1)
# pprint.pprint(result)


def get_messages():
    next_update_id = None

    while True:
        try:
            result = get_updates(next_update_id)
            # print(result['result'][0]['update_id'])
        except ReadTimeout:
            print('timeout')
            continue

        for update in result.get('result', []):
            if 'message' in update:
                yield update['message']
            elif 'edited_message' in update:
                yield update['edited_message']
            else:
                print('Неизвестный апдейт')

        if result.get('result'):
            next_update_id = result['result'][-1].get('update_id') + 1


for message in get_messages():
    # if message['text'] == '/weather':
    #     weather()
    pprint.pprint(message)



