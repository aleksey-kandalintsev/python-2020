# Списки!
from copy import deepcopy

l = [1, 3, 4, 5, 56]
l = []
l = list()
l = [0] * 10
# print(type(l))
# print(l)

l = [
    123,
    [5, 2, 3]
]

# Списки это изменяемый тип данных

vals = [15, 12, 9]
vals2 = vals
# Хранят ссылку на один и тот же объект
# #print(vals2 == vals)
# True
# #print(vals2 is vals)
# True
# #print(id(vals2) == id(vals))
# True
vals2.append(6)
# #print(vals)
# [15, 12, 9, 6]


# Создает новый объект
vals3 = vals.copy()

val3 = vals[:]

vals4 = [vals, vals2]
vals4_copy = vals4.copy()

# #print(vals4[0] is vals)

vals5 = deepcopy(vals3)

l1 = [1, 2]
l2 = [4, 5]
l1.append(l2)
l1[2] is l2

l1.append(l2.copy())
l2.append(54)

l3 = l1.copy()
l3[3].append(4)
l3[0] = 1
l3[0] = 6

l4 = deepcopy(l1)
l4[3].append(4)
l3[3].append(4)
del l3[0]


# 15 - это объект
d = 15
# В vals[0] и d хранится ссылка на один и тот же объект
# #print(id(vals2[0]) == id(d))
# #print(id(vals2[0]) == id(15))

# Удаляет ссылку на объект
d = 15
del d
# #print(vals2[0])

# Срезы
l = [1, 3, 4, 5, 6, 7, 8]
# print(l[0])
# print(l[::-1])
# # print(l.reverse())
# print(l)
# print(l[::-1])
# print(l)


# Можно воспринимать как

# Если шаг = 1 и  или не указан можно читать так
# l[:кол-во э-тов - 1]


# Добавление элементов
# Добавляет переданный объект как один элемент списка, даже если передана
# последователось. Выполняется за константное время
ls = ['F', 's', 3, 3, 4.0]
ls.append('Added')
ls.append([1, 2, 5])

# #print(ls)
# Добавляет элементы из переданной последовательности.
# Длина списка увеличивается на количество элементов из переданной
# последовательности. Выполняется за O(k), где k -  кол-во переданных э-тов
# ls.extend(['E', 'D', 3])
# #print(ls)


# Сложение возвращает новый список, в отличии от предыдущих методов.
new_ls = ls + ['B', 'C']
#print(ls)
# #print(new_ls)


# Определяем длину списка
# print(len(ls))


# Количество элементов в списке
# l = [True, False, True, False, False]
# print(l.count(True))


# Проверка элемента в списке
l = ['This', 'is', 'Sparta']
# print('Leonid' in l)
# print('Sparta' in l)
#
# print(l.index('is'))


# #print(l[1])


# Удаление элементов из списка.
# l = ['one', 'two', 'three', 'four', 'five']
# #print(l)
# #print(l[2])
del l[2:len(l)]

# #print(l[2])
# #print(l)
# #print(l.pop(2))
# #print(l)


# l.remove('one')
# #print(l)

# Сортировка
l = [3, 45, 1.0, True, 56, 3, 1]
so = sorted(l)
# print(so)
# print(l)
# l.sort(reverse=True)
# print(l)


# Список как булевое значение

# #print(bool([]))
# #print(bool([1, 2]))


# генерация списков
new_list2 = [x ** 2 for x in range(4)]

# print(new_list2)


# Генерация списков с условиями
new_list = [x for x in new_list2 if not x % 2]
# print(new_list)


# Эквивалентно:
new_list = []
for x in new_list2:
    if not x % 2:
        new_list.append(x)

# Генерация вложенных списков
n = 5
m = 5
# Матрица, где значение элемента равно произведению индекса строки на индекс столбца
matrix = [[(j, i) for j in range(m)] for i in range(n)]
# print(matrix)

# Эквивалентно
matrix = []
for i in range(m):  # строк
    row = []
    for j in range(n):  # столбцов
        row.append(i * j)
    matrix.append(row)
# print(matrix)
# from pprint import pprint
# pprint(matrix)

# Кортежи
t = (1, 4, 45, 'qwe', 'asd')
# t = (1,)
# t = 1,
# print(type(t))

# print(t[1: -2:1])

# print(t + (34,))

# #print('qwe' in t)


# Конвертация
t_to_l = list((12, 323, 434))
l_to_t = tuple([12, 323, 434])

# #print(t_to_l)
# #print(type(t_to_l))
# #print(l_to_t)
# #print(type(l_to_t))


# Кортеж -  неизменяемый тип данных
example = ()

# Но списки все еще изменяемые
# example = (1, [2, 5])
# print(example)
# x = example[1]
# print(x)
# x.append(5)
# print(example)
# Множественное присваивание
v = (1, 2, 3)
# x, z = v
# print(x)
# print(z)
#print(y)
# print(_)


a, s, d = 3, 4, 5
# #print(a)
# #print(s)
# #print(d)
d = 4,
# #print(d)


# Сеты

s = {1, 2, 3, '12', '4'}
# print(type(s))

# not_set = {}
# print(type(not_set))

# s = set()

sе = {1, 2, 3, 1}
# #print(sе)

# Срезы не будут работать, потому что set - неупорядоченная последовательность
#print(s[1:3])

# Длина
se = {1, 2, 3, 1}
#print(len(sе))


# Добавление элементов
se = {1, 2, 3, 1}
se.add(2)
se.add(5)
se.add(4)
# #print(se)


se.update({2, 4, 10, 11})
# print(se)


# Удаление элементов

# se.remove(4)
# print(se)
# se.discard(4)
# print(se)

# se.remove(4)  # Выдаст ошибку KeyError

# se.discard(10)
# #print(se)


se.clear()

# print(bool(se))

# Конвертация
l = [1, 2, 'asd', 'asd', 1, 'asd']
# print(set(l))

# Операциии со множеством

s_1 = {'a', 'b', 'c', 'd'}
s_2 = {'a', 'e', 'c', 'g', 'l'}

# Объединение
# print(s_1.union(s_2))

# Общие элементы
# print(s_1.intersection(s_2))

# Различия
# print(s_1.difference(s_2))
# print(s_2.difference(s_1))
# print(s_1.symmetric_difference(s_2))

# Оф. Документация:
# https://docs.python.org/3/tutorial/datastructures.html#more-on-lists
# https://docs.python.org/3/tutorial/datastructures.html#sets

# https://pythontutor.ru/lessons/dicts/